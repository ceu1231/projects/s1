function reset(){
	document.getElementById('b').value = "";
	document.getElementById('h').value = "";
	document.getElementById('result').innerHTML = "";
	document.getElementById('btn').removeAttribute('disabled');
}


function calculateArea(btn){
	var b = document.getElementById('b').value;
	var h = document.getElementById('h').value;
	
	if(b != "" && h !=""){
		var par_b=parseInt(b);
		var par_h=parseInt(h);
		
		var area=par_b*par_h;
		
		btn.setAttribute('disabled', 'disabled');
	
		document.getElementById('result').innerHTML = "<center><label style='font-size:26px;'>The Area of Parallelogram is</label> <br /><span class='text-primary' style='font-size:30px;'>"+area+"</span></center>";
	
	}else{
		alert("Please enter something first!");
	}
	
}

